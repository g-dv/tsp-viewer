from algorithms import *

ALGORITHMS = {
    "ELITIST_EVOLUTIONARY": {
        "name": "Elitist Evolutionary",
        "class": ElitistEvolutionary,
    },
    "ARBITRARY_INSERTION_HEURISTIC": {
        "name": "Arbitrary Insertion Heuristic",
        "class": ArbitraryInsertionHeuristic,
    },
    "BRUTE_FORCE": {"name": "Brute Force", "class": BruteForce},
    "KRUSKAL_INSPIRED": {"name": "Kruskal Inspired", "class": KruskalInspired},
    "BRANCH_AND_BOUND": {"name": "Branch and Bound", "class": BranchAndBound},
}

HYPERPARAMETERS = [
    {
        "type": int,
        "algorithm": ALGORITHMS["ELITIST_EVOLUTIONARY"],
        "label": "Nb. evaluations",
        "input": {"value": 1000, "id": "input-max-nb-evaluations"},
    },
    {
        "type": int,
        "algorithm": ALGORITHMS["ELITIST_EVOLUTIONARY"],
        "label": "Population",
        "input": {
            "value": 10,
            "type": "number",
            "min": 1,
            "id": "input-population-size",
        },
    },
    {
        "type": int,
        "algorithm": ALGORITHMS["ELITIST_EVOLUTIONARY"],
        "label": "Offsprings",
        "input": {
            "value": 10,
            "type": "number",
            "min": 2,
            "step": 2,
            "id": "input-offspring-size",
        },
    },
    {
        "type": float,
        "algorithm": ALGORITHMS["ELITIST_EVOLUTIONARY"],
        "label": "Mutation proba.",
        "input": {
            "value": 1.0,
            "type": "number",
            "min": 0.1,
            "max": 1.0,
            "step": 0.1,
            "id": "input-mutation-probability",
        },
    },
]

# converts a value between 0 and 6 (ie. the index of the slider) into a number of cities
NB_CITIES = [5, 10, 25, 50, 100, 500, 36555]
