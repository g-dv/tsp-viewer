#!/usr/bin/env python3

import dash
from dash.dependencies import Input, Output, State

import numpy as np
from time import time
from uuid import uuid1
from math import inf

from data_wrapper import DataWrapper
from options import *
from layout import layout


# app
meta_tags = {"name": "viewport", "content": "width=device-width, initial-scale=1"}
app = dash.Dash(meta_tags=[meta_tags])
app.title = "TSP Viewer"
app.layout = layout

# interface with the data
data = DataWrapper()


def get_nodes(cities):
    """ Transforms the n*2 cities array into an array of Cytoscape nodes """
    return [
        {"data": {"id": i}, "position": {"x": 200 * x, "y": -200 * y}}
        for i, (x, y) in enumerate(cities)
    ]


def get_edges(best_cycle, current_cycle=None):
    """ Transforms the cycle-tuples into an array of Cytoscape edges """
    # if we reached the last step, then print the cycle in bold
    if current_cycle is None:
        current_cycle = best_cycle
    # find what should be the size of our masks
    nb_cities = 1 + max(best_cycle + current_cycle)
    # create an adjacency mask for the the first cycle
    current_neighbors = [[] for _ in range(nb_cities)]
    for i in range(len(current_cycle)):
        for j in (i - 1, (i + 1) % len(current_cycle)):
            current_neighbors[current_cycle[i]].append(current_cycle[j])
    # create an adjacency mask for the the first cycle
    best_neighbors = [[] for _ in range(nb_cities)]
    for i in range(len(best_cycle)):
        for j in (i - 1, (i + 1) % len(best_cycle)):
            best_neighbors[best_cycle[i]].append(best_cycle[j])
    # function to create an edge
    make_edge = lambda a, b: {"data": {"source": a, "target": b}}
    edges = []
    # add all the edges of the first cycle
    for city in range(nb_cities):
        for neighbor in current_neighbors[city]:
            # add the edge
            if neighbor > city:  # else: the edge (neighbor, city) was already added
                edges.append(make_edge(city, neighbor))
                edges[-1]["data"]["is_colored"] = True
            # if the edge exists also on the best cycle...
            if neighbor in best_neighbors[city]:
                # then, make the edge bold..
                edges[-1]["data"]["is_bold"] = True
                # ..and remove it from the other mask because it shouldn't be added twice
                best_neighbors[city].remove(neighbor)
                best_neighbors[neighbor].remove(city)
    # add all the edges of the second cycle that are not in the graph yet
    for city in range(nb_cities):
        for neighbor in best_neighbors[city]:
            if neighbor > city:
                edges.append(make_edge(city, neighbor))
                edges[-1]["data"]["is_bold"] = True
    return edges


def get_trigger_id():
    """ Returns the id of the input that triggered the event """
    return dash.callback_context.triggered[0]["prop_id"].split(".")[0]


@app.callback(
    Output("hyperparameters", "children"),
    [Input("algorithm-select", "value")],
    [State("hyperparameters", "children")],
)
def update_hyperparameters_form(algorithm, children):
    title_style = {"display": "none"}  # hide h4 by default
    for i in range(len(children[1:])):
        if not algorithm or HYPERPARAMETERS[i]["algorithm"] != ALGORITHMS[algorithm]:
            input_style = {"display": "none"}
        else:
            input_style = {}
            title_style = {}
        # hide or show the input group
        children[i + 1]["props"]["style"] = input_style
    # hide or show the title
    children[0]["props"]["style"] = title_style
    return children


@app.callback(
    Output("btn-run", "disabled"),
    [Input("algorithm-select", "value"), Input("distance-select", "value")],
)
def disable_run_button(algorithm, distance_name):
    return not distance_name or not algorithm in ALGORITHMS


@app.callback(
    Output("graph", "elements"),
    [Input("graph-nodes", "data"), Input("graph-edges", "data")],
)
def update_graph(nodes, edges):
    # we should not to draw the edges if the user just changed the nodes
    return nodes if get_trigger_id() == "graph-nodes" else nodes + edges


@app.callback(Output("graph-nodes", "data"), [Input("nb-cities-slider", "value")])
def add_nodes(slider_value):
    return get_nodes(data.get(slider_value))


@app.callback(
    [
        Output("graph-edges", "data"),
        Output("cycle-length", "children"),
        Output("computation-time", "children"),
        Output("controls", "style"),
        Output("current-step", "children"),
        Output("nb-steps", "children"),
        Output("btn-fast-backward", "disabled"),
        Output("btn-step-backward", "disabled"),
        Output("btn-step-forward", "disabled"),
        Output("btn-fast-forward", "disabled"),
        Output("steps", "data"),
    ],
    [
        Input("btn-run", "n_clicks"),
        Input("nb-cities-slider", "value"),
        Input("btn-fast-backward", "n_clicks"),
        Input("btn-step-backward", "n_clicks"),
        Input("btn-step-forward", "n_clicks"),
        Input("btn-fast-forward", "n_clicks"),
    ],
    [
        State("steps", "data"),
        State("algorithm-select", "value"),
        State("distance-select", "value"),
        State("current-step", "children"),
    ]
    + [
        State(hyperparameter["input"]["id"], "value")
        for hyperparameter in HYPERPARAMETERS
    ],
)
def update_graph(
    n_clicks_run,
    slider_value,
    _,
    _1,
    _2,
    _3,
    steps,
    algorithm,
    distance_name,
    current_step,
    *hyperparameters
):
    # check if the slider has just been changed
    trigger_id = get_trigger_id()
    if trigger_id == "nb-cities-slider":
        return (
            [],
            "",
            "",
            {"display": "none"},
            "",
            "",
            dash.no_update,
            dash.no_update,
            dash.no_update,
            dash.no_update,
            [],
        )
    if trigger_id == "btn-fast-backward":
        current_step = 1
    if trigger_id == "btn-step-backward" and current_step > 1:
        current_step -= 1
    if trigger_id == "btn-step-forward":
        current_step += 1
    if trigger_id == "btn-fast-forward":
        current_step = len(steps)
    if trigger_id != "btn-run":
        if current_step >= len(steps):
            return (
                get_edges(steps[-1][3]),
                "Length: {0:.2f} km".format(steps[-1][2]),
                dash.no_update,
                dash.no_update,
                len(steps),
                len(steps),
                False,
                False,
                True,
                True,
                steps,
            )
        length, cycle, best_length, best_cycle = steps[current_step - 1]
        best_length = best_length if best_length else inf
        return (
            get_edges(best_cycle, cycle),
            "Length: {0:.0f} - Best so far: {1:.0f}".format(length, best_length),
            dash.no_update,
            dash.no_update,
            current_step,
            len(steps),
            current_step == 1,
            current_step == 1,
            False,
            False,
            steps,
        )
    # convert hyperparameters inputs to adequate types
    hyperparameters = [
        HYPERPARAMETERS[i]["type"](e)
        for i, e in enumerate(hyperparameters)
        if HYPERPARAMETERS[i]["algorithm"] == ALGORITHMS[algorithm]
    ]
    # run the algorithm
    cities = data.get(slider_value)
    start_time = time()
    algorithm = ALGORITHMS[algorithm]["class"](cities, distance_name)
    instance = algorithm.run(*hyperparameters)
    steps = [
        (length, cycle, best_length, best_cycle)
        for (length, cycle, best_length, best_cycle) in instance
    ]
    return (
        get_edges(steps[-1][3]),
        "Length: {0:.2f} km".format(steps[-1][2]),
        "({0:.2f} sec)".format(time() - start_time),
        {"display": "block"},
        len(steps),
        len(steps),
        False,
        False,
        True,
        True,
        steps,
    )


if __name__ == "__main__":
    app.run_server()
