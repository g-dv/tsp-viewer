# TSP Viewer

Python dashboard to try and visualize algorithms for the [Travelling Salesman Problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem).

## Motivation

I started this project for three main reasons:
 * There were some algorithms that I wanted to try.
 * I wanted to discover and try Dash
 * It is, in my opinion, a cool little project.

## Technologies used

The project is written entirely in Python 3 and uses [Plotly Dash](https://plot.ly/dash/).

The algorithms are described in the `algorithms.py` file.

## Usage

Launch `app.py` with Python 3. The dependencies can be installed with pip using the `requirements.txt` file. For example with the line:
```
python3 -m pip install -r requirements.txt
```

### Important notice

Be aware that I did not add safeguards. Use this software at your own risk. For example, running the naive brute force algorithm which runs in $`O(n!)`$ with $`n=25`$ cities is likely to use all of your system resources.

Moreover, just because it runs in the browser does not mean that it's fit to be hosted for online use: it's not.

## Screenshot

![screenshot](assets/screenshot.png)

## Benchmark

|                                    | 5 cities            | 5 cities            | 25 cities           | 50 cities            | 100 cities           |
| ---------------------------------- | ------------------- | ------------------- | ------------------- | -------------------- | -------------------- |
| Naive Brute Force                  | 1'757 km<br>00.00 s | 2'600 km<br>03.35 s |                     |                      |                      |
| Branch and Bound                   | 1'757 km<br>00.00 s | 2'600 km<br>00.40 s |                     |                      |                      |
| Arbitrary Insertion                | 1'757 km<br>00.00 s | 2'600 km<br>00.00 s | 3'906 km<br>00.01 s | 4'585 km<br>00.04 s  | 5'867 km<br>00.26 s  |
| Kruskal Inspired                   | 2'071 km<br>00.00 s | 2'600 km<br>00.00 s | 4'308 km<br>00.01 s | 5'035 km<br>00.02 s  | 5'735 km<br>00.10 s  |
| Elitist Evolutionary 100 eval.     | 1'757 km<br>00.02 s | 3'238 km<br>00.04 s | 7'508 km<br>00.05 s | 1'5420 km<br>00.08 s | 32'708 km<br>00.17 s |
| Elitist Evolutionary 1'000 eval.   | 1'757 km<br>00.18 s | 2'600 km<br>00.26 s | 5'134 km<br>00.36 s | 1'1608 km<br>00.62 s | 23'744 km<br>01.13 s |
| Elitist Evolutionary 10'000 eval.  | 1'757 km<br>01.70 s | 2'600 km<br>02.23 s | 4'155 km<br>03.32 s | 5'875 km<br>05.73 s  | 14'646 km<br>10.44 s |
| Elitist Evolutionary 100'000 eval. | 1'757 km<br>16.52 s | 2'600 km<br>20.42 s | 3'770 km<br>33.49 s | 5'677 km<br>54.47 s  | 7'664  km<br>97.31 s |

**Notice:** The results for the stochastic algorithms show the average of 10 runs.