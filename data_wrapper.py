import pandas as pd
from options import *


class DataWrapper:
    def __init__(self):
        self.data = (
            pd.read_csv("data/cities_france.csv")
            .sort_values(by="size", ascending=False)
            .drop(["size"], axis=1)
        )

    def get(self, level):
        return self.data.head(NB_CITIES[level]).values
