import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_cytoscape as cyto
from options import *


layout = html.Div(
    [
        html.Div(
            [
                html.H2("TSP Viewer"),
                html.P(
                    [
                        "Python dashboard for the ",
                        html.A(
                            "TSP",
                            href="https://en.wikipedia.org/wiki/Travelling_salesman_problem",
                        ),
                    ],
                    className="lead",
                ),
                html.P(
                    [
                        html.A(
                            [html.I(className="mr-2 fa fa-info-circle"), "Readme"],
                            href="https://gitlab.com/g-dv/tsp-viewer/-/blob/master/README.md",
                        ),
                        html.A(
                            [html.I(className="mr-2 fab fa-gitlab"), "Repo"],
                            href="https://gitlab.com/g-dv/tsp-viewer",
                            className="ml-3",
                        ),
                    ]
                ),
                html.H3("Data"),
                html.P(
                    "The dataset contains the n-most populated cities in mainland France"
                ),
                dcc.Slider(
                    id="nb-cities-slider",
                    min=0,
                    max=6,
                    marks={i: {"label": str(e)} for i, e in enumerate(NB_CITIES)},
                    value=2,
                    dots=True,
                ),
                dbc.Select(
                    options=[
                        {"label": "Euclidian distance (2D)", "value": "euclidian"},
                        {"label": "Haversine distance (lat/lng)", "value": "haversine"},
                    ],
                    id="distance-select",
                ),
                html.H3("Algorithm"),
                dbc.InputGroup(
                    [
                        dbc.Select(
                            options=[
                                {"label": ALGORITHMS[key]["name"], "value": key}
                                for key in ALGORITHMS
                            ],
                            id="algorithm-select",
                        ),
                        dbc.InputGroupAddon(
                            dbc.Button(html.I(className="fa fa-play"), id="btn-run"),
                            addon_type="append",
                        ),
                    ]
                ),
                html.Div(
                    [html.H4("Hyperparameters", style={"display": "none"})]
                    + [
                        dbc.InputGroup(
                            [
                                dbc.InputGroupAddon(e["label"], addon_type="prepend"),
                                dbc.Input(**e["input"]),
                            ],
                            style={"display": "none"},
                        )
                        for e in HYPERPARAMETERS
                    ],
                    id="hyperparameters",
                ),
                html.Div(
                    [
                        html.H4("Controls"),
                        dbc.ButtonGroup(
                            [
                                dbc.Button(
                                    html.I(className="fa fa-fast-backward"),
                                    id="btn-fast-backward",
                                ),
                                dbc.Button(
                                    html.I(className="fa fa-step-backward"),
                                    id="btn-step-backward",
                                ),
                                dbc.Button(
                                    [
                                        html.Span("", id="current-step"),
                                        "/",
                                        html.Span("", id="nb-steps"),
                                    ],
                                    disabled=True,
                                ),
                                dbc.Button(
                                    html.I(className="fa fa-step-forward"),
                                    id="btn-step-forward",
                                ),
                                dbc.Button(
                                    html.I(className="fa fa-fast-forward"),
                                    id="btn-fast-forward",
                                ),
                            ]
                        ),
                    ],
                    id="controls",
                    style={"display": "none"},
                ),
            ],
            id="sidebar",
        ),
        html.Div(
            [
                cyto.Cytoscape(
                    id="graph",
                    layout={"name": "preset", "animate": True, "padding": 50},
                    style={"width": "100%", "height": "100vh"},
                    elements=[],
                    stylesheet=[
                        {
                            "selector": "node",
                            "style": {
                                "background-color": "#2B3E50",
                                "width": 30,
                                "height": 30,
                            },
                        },
                        {
                            "selector": "edge[?is_colored]",
                            "style": {"line-color": "#DF691A"},
                        },
                        {"selector": "edge[?is_bold]", "style": {"width": "10"}},
                    ],
                    userPanningEnabled=False,
                    autolock=True,
                    autounselectify=True,
                ),
                dcc.Store(data=[], id="graph-nodes"),
                dcc.Store(data=[], id="graph-edges"),
                dcc.Store(data=[], id="steps"),
                html.P(id="cycle-length"),
                html.P(id="computation-time"),
            ],
            id="page-content",
        ),
    ]
)
