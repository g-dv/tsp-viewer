import numpy as np
from numpy.random import rand, choice, randint
from math import radians, sin, cos, atan2, sqrt, inf
from itertools import permutations


class Algorithm:
    def __init__(self, cities, distance="euclidian"):
        self.cities = cities
        self.nb_cities = len(cities)
        self.nb_evaluations = 0  # number of time "get_length" was called
        if distance == "euclidian":
            self._get_distance = self._get_euclidian_distance
        elif distance == "haversine":
            self._get_distance = self._get_haversine_distance
        else:
            raise ValueError("Unknown distance formula")
        self.distances = self._get_distances()

    def run(self, *args):
        """ Let "c" be the cycle that is being considered and "b" the best cycle found so far.
            Let len(x) be the length of a given cycle x
            Then, the algorithm will yield (len(c), c, len(b), b) until it ends.
            It will then yield (None, None, len(b), b).
        """
        pass

    def _get_length(self, permutation):
        """ Returns the length of the TSP tour
            through all cities in the order
            given in permutation.
        """
        self.nb_evaluations += 1
        length = 0
        for i in range(len(permutation)):
            length += self.distances[permutation[i - 1]][permutation[i]]
        return length

    def _get_distances(self, type="euclidian"):
        """ 
            Returns a distance matrix with the symmetric
            pairwise Euclidean distances between the n
            cities in the (n, 2) numpy.array `cities`.
        """
        nb_cities = len(self.cities)
        distances = np.zeros((nb_cities, nb_cities))
        for i in range(nb_cities):
            for j in range(nb_cities):
                distances[i][j] = self._get_distance(self.cities[i], self.cities[j])
        return distances

    def _get_euclidian_distance(self, a, b):
        return np.linalg.norm(a - b)

    def _get_haversine_distance(self, a, b):
        """ Compute the distance between points of a sphere.
            Used with a and b being a pair of (longitude, latitude).
        """
        lng_a, lat_a, lng_b, lat_b = *a, *b
        RADIUS = 6371  # radius of the Earth in km
        lat_difference = radians(lat_b - lat_a)
        lng_difference = radians(lng_b - lng_a)
        tmp = sin(lat_difference / 2) * sin(lat_difference / 2) + cos(
            radians(lat_a)
        ) * cos(radians(lat_b)) * sin(lng_difference / 2) * sin(lng_difference / 2)
        return RADIUS * 2 * atan2(sqrt(tmp), sqrt(1 - tmp))


class ElitistEvolutionary(Algorithm):
    def run(self, max_nb_evaluations, population_size, offspring_size, mutation_proba):
        """ Complexity (including init): O(n²+mn) with: n=nb_cities, m=max_nb_evaluations
            (if we neglect the size of the population and of the offsprings)
            At each generation, we select two individuals among the population, we cross
            them to generate two children, we mutate the children, and add them to the
            population if they are better.
            `nb_evaluations` maximal nb of function evaluations performed
            `population_size` nb of solutions used to create offsprings
            `offspring_size` nb of new candidates at each generation; it should be even
        """
        # Initialization
        population = self._init_population(population_size)
        lengths = np.array([self._get_length(cycle) for cycle in population])

        # while we have reached the limit of evaluations...
        while self.nb_evaluations <= max_nb_evaluations:

            # Offspring creation
            for i in range(offspring_size // 2):
                # choose 2 parents randomly with replacement
                parents = population[choice(len(population), 2)]
                # crossover to create 2 children
                children = self._crossover(*parents)
                # mutate
                children = [
                    self._mutate(child) if rand() < mutation_proba else child
                    for child in children
                ]
                # add to population
                for cycle in children:
                    population = np.append(population, [cycle], axis=0)
                    length = self._get_length(cycle)
                    lengths = np.append(lengths, [length])
                    # yield state
                    if length < lengths[0]:
                        yield length, cycle, length, cycle
                    else:
                        yield length, cycle, lengths[0], population[0]

            # Environmental selection: keep only the best
            indices = lengths.argsort()  # sort the two vectors using `lengths`..
            population = population[indices][0:population_size]  # ..and keep only the..
            lengths = lengths[indices][0:population_size]  # ..first ones (=the best)..

        yield None, None, lengths[0], population[0]

    def _init_population(self, size):
        """returns `size` random permutations of the cities (=random paths)"""
        # get a random permuation by sorting a random numpy array
        return np.array([rand(self.nb_cities).argsort() for i in range(size)])

    def _mutate(self, permutation):
        """ Mutates the given permutation by swapping two random cities.
        """
        i, j = randint(self.nb_cities), randint(self.nb_cities)
        permutation[i], permutation[j] = permutation[j], permutation[i]
        return permutation

    def _cut_and_copy(self, i, j, parent1, parent2):
        """ Performs the crossover operation in one way (ie. generates only one child)
            using the given crossover points i and j. Returns the child generated.
        """
        # init
        child = -np.ones(self.nb_cities, dtype=int)
        # copy middle parts from parents
        child[i : j + 1] = parent1[i : j + 1]
        parent_begin_index = np.nonzero(parent2 == child[j])[0][0]
        # fill the right part
        end_index = self._fill(j, self.nb_cities, child, parent2, parent_begin_index)
        # fill the left part
        end_index = self._fill(-1, i, child, parent2, end_index)
        return child

    def _fill(self, begin, end, child, parent, parent_begin_index):
        while begin < end - 1:
            begin = (begin + 1) % self.nb_cities
            while parent[parent_begin_index] in child:
                parent_begin_index = (parent_begin_index + 1) % self.nb_cities
            child[begin] = parent[parent_begin_index]
        return parent_begin_index

    def _crossover(self, parent1, parent2):
        """ Creates two children from the two given permutations by order 1 crossover.
        """
        # randomly pick the two crossover points
        i, j = np.sort(randint(0, self.nb_cities - 1, 2))
        return (
            self._cut_and_copy(i, j, parent1, parent2),
            self._cut_and_copy(i, j, parent2, parent1),
        )


class ArbitraryInsertionHeuristic(Algorithm):
    def run(self):
        """ Complexity: O(n²)  with: n=nb_cities
            We start with an empty path and add the cities one by one. To decide where
            to insert each city, we find the position for which the total length
            increases the least.
        """
        solution = [0]
        for i in range(1, self.nb_cities):
            best_length = inf
            for j in range(len(solution)):
                candidate = solution[:j] + [i] + solution[j:]
                length = self._get_length(candidate)
                if length < best_length:
                    best_candidate, best_length = candidate, length
                yield length, candidate, best_length, best_candidate
            solution = best_candidate
        yield None, None, best_length, solution


class BruteForce(Algorithm):
    def run(self):
        """ Complexity: O(n!)  with n=nb_cities
            We compute all the possible solutions and find the best one.
        """
        best_length = inf
        for permutation in permutations(range(1, self.nb_cities)):
            cycle = [0] + list(permutation)
            length = self._get_length(cycle)
            if length < best_length:
                best_length, best_cycle = length, cycle
            yield length, cycle, best_length, best_cycle
        yield None, None, best_length, best_cycle


class KruskalInspired(Algorithm):
    def run(self):
        """ Complexity: O(n²logn)  with n=nb_cities (in theory, because in practice our
            implementation of the union-find sucks)
            This is a modified version of the Kruskal algorithm, where we put limitations
            on the degree of the nodes, and where we lift the anti-cycle rule at the very
            end in order to close the loop.
            The characteristic of this algorithm is that the median distance between the
            nodes will be very low, but there will be some very costly errors at the end
            since the algorithm doesn't make compromises.
        """
        # make a list of all possible edges
        edges = [
            (i, j, self.distances[i][j])
            for i in range(self.nb_cities)
            for j in range(i)
        ]
        # sort the edges by distance
        edges.sort(key=lambda x: x[2])
        # select the edges that will be part of the graph
        neighbors = [[] for i in range(self.nb_cities)]
        selected_edges = []
        self.subsets = [[node] for node in range(self.nb_cities)]
        for edge in edges:
            # select the edge if:
            #  - it does not cause a vertex to have a degree of three or more
            #  - it does not form a cycle (unless it's the last edge)
            subsets = self._find(edge[0]), self._find(edge[1])
            if (len(neighbors[edge[0]]) < 2 and len(neighbors[edge[1]]) < 2) and (
                subsets[0] != subsets[1] or len(selected_edges) == self.nb_cities - 1
            ):
                neighbors[edge[0]].append(edge[1])
                neighbors[edge[1]].append(edge[0])
                selected_edges.append(edge)
                self._union(subsets)

        # reconstruct the path using the edges
        solution = [0, neighbors[0][0]]
        for _ in range(self.nb_cities - 2):
            a, b = neighbors[solution[-1]]
            solution.append(a if a != solution[-2] else b)
        yield None, None, self._get_length(solution), solution

    def _find(self, node):
        # NB: This implementation of Union-Find is definitely not optimal
        for i, subset in enumerate(self.subsets):
            if node in subset:
                return i
        return None

    def _union(self, subsets):
        self.subsets[subsets[0]] += self.subsets[subsets[1]]
        self.subsets[subsets[1]] = []


class BranchAndBound(Algorithm):
    def run(self):
        """ Complexity: O(n!)  with n=nb_cities
            This is a classic branch and bound algorithm: we are doing a recursive search
            but we stop the recursion before reaching the end of the candidate solution
            if we meet conditions that prove the current candidate to be non-optimal.
            These conditions are:
             - if the length of the path we are making is already greater than our best
               solution
             - if the path we are making has two edges that intersect
            Indeed, a cycle where two edges intersect is necessarily sub-optimal:
                Let's say that we found an Hamiltonian cycle where two edges intersect.
                If we remove these edges (let's call them AB and CD) from the graph,
                we can still add two different edges to close the cycle again (either AD
                and BC or AC and BD).
                Now, since the diagonals of a quadrilateral ACBD are longer than any of
                its sides, then the edges that we added must be shorter than AB and CD.
                Thus, the resulting graph is a better solution.
                Thus, any solution with crossed edges is not optimal.
        """
        self.best_length = inf
        self.best_cycle = []
        self.cycle = [0]
        self.remaining_cities = list(range(1, self.nb_cities))

        yield from self._backtrack(0)
        yield None, None, self.best_length, self.best_cycle

    def _are_intersecting(self, a, b, c, d):
        are_counter_clock = lambda p, q, r: (
            (q[0] - p[0]) * (r[1] - q[1]) < (q[1] - p[1]) * (r[0] - q[0])
        )
        tmp = are_counter_clock(a, c, d) != are_counter_clock(b, c, d)
        return tmp and are_counter_clock(a, b, c) != are_counter_clock(a, b, d)

    def _backtrack(self, length):
        cycle_length = length + self.distances[self.cycle[-1]][self.cycle[0]]
        yield cycle_length, self.cycle[:], self.best_length, self.best_cycle
        # is the cycle too long?
        if cycle_length >= self.best_length:
            return
        # is the cycle finished?
        if len(self.cycle) == self.nb_cities:
            self.best_length = cycle_length
            self.best_cycle = self.cycle[:]
            return
        # is the last edge crossing the path?
        if len(self.cycle) > 3:
            new_edge = [self.cities[self.cycle[-1]], self.cities[self.cycle[-2]]]
            for i in range(len(self.cycle) - 2):
                if self._are_intersecting(
                    *new_edge,
                    self.cities[self.cycle[i]],
                    self.cities[self.cycle[i + 1]]
                ):
                    return
        # for each possible next city, add it to the cycle temporarily and backtrack
        self.cycle.append(None)
        for i, city in enumerate(self.remaining_cities):
            if city:  # cities that are already in the graph
                self.remaining_cities[i] = None  # are set to "None"
                self.cycle[-1] = city
                yield from self._backtrack(
                    length + self.distances[city][self.cycle[-2]]
                )
                self.remaining_cities[i] = city
        self.cycle.pop()
